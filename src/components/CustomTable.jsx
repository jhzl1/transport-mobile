import { Table, Button } from "antd";
import useSearchTable from "../hooks/useSearchTable";
import axios from "../helpers/axiosInstance";
import { useEffect, useState, useCallback } from "react";
import { BASE_URL } from "../services/api.js";
import { useLocation, useHistory } from "react-router-dom";
import { checkSearchTable } from "../helpers/checkSearchTable";

const CustomTable = ({
  endpoint,
  rowKey = "id",
  configColumns,
  limitDirection,
}) => {
  const { search } = useLocation();
  let query = new URLSearchParams(search);
  let page = parseInt(query.get("page")) || 1;

  const history = useHistory();
  const [data, setData] = useState([]);
  const [total, setTotal] = useState(0);
  const [loading, setLoading] = useState(false);
  const [sizePage, setSizePage] = useState(10);
  const [pageNumber, setPageNumber] = useState(page - 1);
  const [withParams, setWithParams] = useState(false);

  const getColumnSearchProps = useSearchTable();

  const getData = useCallback(
    async (params) => {
      setLoading(true);
      history.push({ search: `?page=${pageNumber + 1}` });
      try {
        const res = await axios.post(
          `${BASE_URL}/${endpoint}`,
          params
            ? {
                number: pageNumber,
                size: sizePage,
                filters: [params],
              }
            : {
                number: pageNumber,
                size: sizePage,
              }
        );
        setData(res.data.result);
        console.log(res);
        setTotal(res.data.count);
      } catch (error) {
        console.log(error);
      }
      setLoading(false);
    },
    [endpoint, pageNumber, sizePage, history]
  );

  const handleTableChange = async (pages, filters, sorter) => {
    // getData({
    //   sortField: sorter.field,
    //   sortOrder: sorter.order,
    //   pages,
    //   ...filters,
    // });
    console.log(filters);
    const search = checkSearchTable(filters);
    console.log(search);
    if (search) {
      getData(search);
      setPageNumber(0);
      setWithParams(true);
    } else {
      setPageNumber(pages.current - 1);
      setSizePage(pages.pageSize);
      // getData();
    }
  };

  const clearFilters = () => {
    setPageNumber(0);
    setSizePage(10);
    getData();
  };

  useEffect(() => {
    let mounted = true;

    if (mounted) {
      getData();
    }

    return () => {
      setData([]);
      mounted = false;
    };
  }, [getData]);

  return (
    <>
      {/* <div className="mt-3">
        {withParams && (
          <Button onClick={() => clearFilters()}>Primary Button</Button>
        )}
      </div> */}
      <Table
        rowKey={rowKey}
        className="mt-3 table"
        scroll={{ x: "fit-content" }}
        columns={configColumns(getColumnSearchProps)}
        expandable={{
          expandedRowRender: (record) => (
            <>
              {limitDirection && (
                <p className="mx-5 my-1">Dirección: {record.fiscalAddress}</p>
              )}
              <p className="mx-5 my-1">StrongId: {record.strongId}</p>
            </>
          ),
        }}
        dataSource={data}
        pagination={{ total: total, defaultCurrent: page }}
        loading={loading}
        onChange={handleTableChange}
        size="small"
      />
    </>
  );
};

export default CustomTable;
