import ClousureList from "../../pages/ClousureList";
import Home from "../../pages/Home";
import PosList from "../../pages/PosList";
import DetailPos from "../../pages/DetailPos";
import { routes } from "./routes";
import DetailUser from "../../pages/DetailUser";

const { home, clousureList, posList, posDetail, userProfile } = routes;

export const dirRoutes = [
  { id: 0, component: Home, path: home },
  { id: 1, component: ClousureList, path: clousureList },
  { id: 2, component: PosList, path: posList },
  { id: 3, component: DetailPos, path: posDetail() },
  { id: 4, component: DetailUser, path: userProfile },
];
