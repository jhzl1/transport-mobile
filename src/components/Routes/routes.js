export const routes = {
  login: "/",
  home: "/home",
  clousureList: "/clousures",
  posList: "/posList",
  posDetail: (strongId = "/:strongId") => `/posList/${strongId}`,
  userProfile: "/userProfile",
};
