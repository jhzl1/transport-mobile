import { Route, Redirect } from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import { routes } from "./routes";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const auth = useAuth();

  const { login } = routes;

  return (
    <Route {...rest}>
      {auth.testLogin ? <Component /> : <Redirect to={login} />}
    </Route>
  );
};

export default PrivateRoute;
