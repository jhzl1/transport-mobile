import { Tooltip } from "antd";
import * as FiIcons from "react-icons/fi";
import useAuth from "../hooks/useAuth";

const UserHeader = () => {
  const auth = useAuth();

  const handleClick = (e) => {
    e.preventDefault();
    auth.logout();
  };

  return (
    <div className="my-2 mx-2 d-flex align-items-center">
      <div className="mx-2">
        <Tooltip placement="bottom" title="Cerrar Sesión">
          <a
            href="/"
            onClick={(e) => handleClick(e)}
            className="logout-link p-2"
          >
            <FiIcons.FiLogOut className="fs-3" />
          </a>
        </Tooltip>
      </div>
    </div>
  );
};

export default UserHeader;
