import logo from "../assets/img/unidigital-logo-new.png";

const HeaderSidebar = () => {
  return (
    <div className="px-3 py-3">
      <img src={logo} alt="Unidigital" width="170px" className="ms-1" />
    </div>
  );
};

export default HeaderSidebar;
