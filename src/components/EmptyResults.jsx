import emptyImg from "../assets/img/box.png";

const EmptyResults = () => {
  return (
    <div className="d-flex flex-column align-items-center p-3">
      <img src={emptyImg} alt="No hay resultados" className="empty-box mb-4" />
      <span className="fs-3">
        No se encontraron resultados para esta entidad.
      </span>
    </div>
  );
};

export default EmptyResults;
