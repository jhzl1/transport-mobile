import { Form, Select } from "formik-antd";

const InputSelect = ({ name, options, placeholder }) => {
  const { Option } = Select;

  return (
    <Form.Item name={name} label={placeholder} className="input w-100 me-2">
      <Select name={name} placeholder={placeholder}>
        {options.map((item) => (
          <Option value={item.strongId} key={item.id}>
            {item.name}
          </Option>
        ))}
      </Select>
    </Form.Item>
  );
};

export default InputSelect;
