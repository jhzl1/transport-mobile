import { Form, Input } from "formik-antd";

const InputText = ({
  name,
  placeholder,
  disabled,
  maxLength,
  limitNumber,
  limitSpace,
}) => {
  const handleKeyDown = (e) => {
    if (e.key === " ") {
      e.preventDefault();
    }
  };

  return (
    <Form.Item name={name} label={placeholder} className="input w-100 me-2">
      <Input
        name={name}
        className="w-100"
        placeholder={placeholder}
        maxLength={maxLength}
        allowClear
        type={limitNumber && "number"}
        onKeyDown={limitSpace && handleKeyDown}
        disabled={disabled && disabled}

        // onBeforeInput={(e) => e.target.value.trim()}
      />
    </Form.Item>
  );
};

export default InputText;
