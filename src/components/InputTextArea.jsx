import { Form, Input } from "formik-antd";
const InputTextArea = ({ name, placeholder }) => {
  const { TextArea } = Input;

  return (
    <Form.Item name={name} label={placeholder} className="input w-100 me-2">
      <TextArea
        name={name}
        placeholder={placeholder}
        autoSize={{ minRows: 5, maxRows: 6 }}
        showCount
        maxLength={250}
        allowClear
      />
    </Form.Item>
  );
};

export default InputTextArea;
