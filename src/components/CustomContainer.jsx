import { Container, Row, Col } from "react-bootstrap";
import { motion } from "framer-motion";
import { desktopVariants } from "../data/dataAnimation";

const CustomContainer = ({ children }) => {
  return (
    <motion.div
      variants={desktopVariants}
      initial="initial"
      animate="enter"
      exit="exit"
    >
      <Container className="desktop-container mt-2 mb-5 p-1" fluid>
        <Row>
          <Col>{children}</Col>
        </Row>
      </Container>
    </motion.div>
  );
};

export default CustomContainer;
