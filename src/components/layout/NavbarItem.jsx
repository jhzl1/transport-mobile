import { NavLink } from "react-router-dom";

const NavbarItem = ({ name, icon, path }) => {
  return (
    <NavLink to={path} className="link-navbar" activeClassName="selected">
      <div className="d-flex align-items-center flex-column item-navbar py-1">
        <div className="icon-navbar">{icon}</div>
        <span className="fw-bold">{name}</span>
      </div>
    </NavLink>
  );
};

export default NavbarItem;
