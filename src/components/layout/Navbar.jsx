import { itemsNavbar } from "../config/navbar/configItemsNavbar";
import ItemNavbar from "./NavbarItem";

const Navbar = () => {
  return (
    <div className="fix-navbar d-flex justify-content-evenly">
      {itemsNavbar.map((item) => (
        <ItemNavbar
          key={item.id}
          name={item.name}
          icon={item.icon}
          path={item.path}
        />
      ))}
    </div>
  );
};

export default Navbar;
