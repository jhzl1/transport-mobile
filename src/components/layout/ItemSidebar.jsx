import { Link } from "react-router-dom";
import * as AiIcons from "react-icons/ai";
import { useState } from "react";
import { motion, AnimatePresence } from "framer-motion";

const ItemSidebar = ({ path, title, icon, subnav: items }) => {
  const [subnav, setSubnav] = useState(false);

  const showSubnav = () => {
    setSubnav(!subnav);
  };

  return (
    <>
      <motion.div
        className="item-sidebar w-100 d-flex justify-content-between"
        onClick={items && showSubnav}
      >
        {path ? (
          <Link to={path} className="link-sidebar w-100 p-3">
            <div>
              <span className="me-2 icon-link">{icon}</span> {title}
            </div>
          </Link>
        ) : (
          <div className="link-sidebar w-100 p-3">
            <span className="me-2 icon-link">{icon}</span> {title}
          </div>
        )}

        {items && subnav ? (
          <AiIcons.AiFillCaretUp className="icon-subnav me-3" />
        ) : items ? (
          <AiIcons.AiFillCaretDown className="icon-subnav me-3" />
        ) : null}
      </motion.div>

      <AnimatePresence initial={false}>
        {subnav &&
          items.map((item, index) => (
            <motion.div
              initial="collapsed"
              animate="open"
              exit="collapsed"
              variants={{
                open: { opacity: 1, height: "auto" },
                collapsed: { opacity: 0, height: 0 },
              }}
              transition={{
                open: {
                  transition: { staggerChildren: 0.07, delayChildren: 0.2 },
                },
                collapsed: {
                  transition: { staggerChildren: 0.05, staggerDirection: -1 },
                },
                duration: 0.2,
              }}
              className="subnav"
              key={index}
            >
              <motion.div
                // transition={{ duration: 0.2 }}
                className="box-link-subnav w-100 d-flex"
              >
                <Link to={item.path} className="link-subnav w-100 p-3">
                  {item.name}
                </Link>
              </motion.div>
            </motion.div>
          ))}
      </AnimatePresence>
    </>
  );
};

export default ItemSidebar;
