import { Container, Row, Col } from "react-bootstrap";
import UserHeader from "../UserHeader";
import useAuth from "../../hooks/useAuth";
import Navbar from "./Navbar";
import { useTitlePage } from "../../hooks/useTitlePage";

const Desktop = ({ children, location }) => {
  const auth = useAuth();

  const titleHeader = useTitlePage();

  if (auth.testLogin !== true) {
    return <>{children}</>;
  } else {
    return (
      <>
        <Container fluid>
          <Row id="main-right">
            <Col className="ps-0 pe-0 h-100">
              <Row id="topbar" className="m-0">
                <div className="header-desktop p-2  d-flex justify-content-between">
                  <div className="float-right w-50 py-2 px-3 title-desktop">
                    <h2 className="fw-bold m-0 p-0">{titleHeader}</h2>
                  </div>

                  <UserHeader />
                </div>
              </Row>
              <Row id="main-content" className="mx-0 flex-wrap">
                <Col className="ps-0 pe-0">{children}</Col>
              </Row>
            </Col>
          </Row>
        </Container>
        <Navbar />
      </>
    );
  }
};

export default Desktop;
