import CustomForm from "../components/CustomForm";
import { Formik } from "formik";
import InputPassword from "../components/InputPassword";
import { SubmitButton } from "formik-antd";
import { Col } from "react-bootstrap";
import { validationEditCurentPassword } from "./config/forms/configEditCurrentPassword";

const FormChangePassword = ({ handleSubmit }) => {
  return (
    <Formik
      initialValues={{
        contraseñaVieja: "",
        nuevaContraseña: "",
        repetirNuevaContraseña: "",
      }}
      validationSchema={validationEditCurentPassword}
      onSubmit={handleSubmit}
    >
      <CustomForm>
        <InputPassword
          name="contraseñaVieja"
          placeholder="Contraseña Anterior"
        />
        <InputPassword name="nuevaContraseña" placeholder="Nueva Contraseña" />
        <InputPassword
          name="repetirNuevaContraseña"
          placeholder="Repita Nueva"
        />

        <Col lg={12} className="d-flex justify-content-center">
          <SubmitButton type="primary" className="m-2">
            Guardar Cambios
          </SubmitButton>
        </Col>
      </CustomForm>
    </Formik>
  );
};

export default FormChangePassword;
