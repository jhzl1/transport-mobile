import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

const SkeletonDescriptionPos = () => {
  return (
    <SkeletonTheme color="#e0e0e0" highlightColor="#ddd">
      <Skeleton height={110} className="mb-5" />
      <div className="d-flex justify-content-center mb-5">
        <div className="w-50 d-flex justify-content-center">
          <Skeleton className="mx-5" circle={true} height={200} width={200} />
        </div>
        <div className="w-50 d-flex justify-content-center">
          <Skeleton className="mx-5" circle={true} height={200} width={200} />
        </div>
      </div>
      <Skeleton count={5} height={55} />
    </SkeletonTheme>
  );
};

export default SkeletonDescriptionPos;
