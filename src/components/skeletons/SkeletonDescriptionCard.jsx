import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

const SkeletonDescriptionCard = () => {
  return (
    <SkeletonTheme color="#e0e0e0" highlightColor="#ddd">
      <Skeleton height={110} className="mb-5" />
      <div className="d-flex justify-content-center mb-5">
        <Skeleton circle={true} height={200} width={200} />
      </div>
      <Skeleton count={5} height={55} />
    </SkeletonTheme>
  );
};

export default SkeletonDescriptionCard;
