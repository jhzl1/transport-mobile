import { Descriptions } from "antd";
import UserImg from "../assets/img/user.png";

const DescriptionUser = ({
  name,
  email,
  fiscalRegistry,
  phoneNumber,
  direction,
}) => {
  return (
    <>
      <div className="d-flex w-100 justify-content-center">
        <div className="image-detail">
          <img src={UserImg} alt="Centro de recarga" width="100px" />
        </div>
      </div>
      <Descriptions title="Detalles de Usuario" column={1} bordered>
        <Descriptions.Item label="Nombre Completo">{name}</Descriptions.Item>
        <Descriptions.Item label="Email">{email}</Descriptions.Item>
        <Descriptions.Item label="RIF">{fiscalRegistry}</Descriptions.Item>
        <Descriptions.Item label="Teléfono">{phoneNumber}</Descriptions.Item>
        <Descriptions.Item label="Dirección">{direction}</Descriptions.Item>
      </Descriptions>
    </>
  );
};

export default DescriptionUser;
