import { Container, Row, Col } from "react-bootstrap";
import { motion } from "framer-motion";
import { desktopVariants } from "../data/dataAnimation";
import { PageHeader } from "antd";
import { useHistory } from "react-router";

const CustomContainerForm = ({ title, children }) => {
  const history = useHistory();

  return (
    <motion.div
      variants={desktopVariants}
      initial="initial"
      animate="enter"
      exit="exit"
    >
      <Container className="desktop-container my-5 p-4" fluid>
        <Row>
          <Col>
            <PageHeader
              ghost={false}
              onBack={() => history.goBack()}
              title={title}
              className="site-page-header mb-3"
            />

            {children}
          </Col>
        </Row>
      </Container>
    </motion.div>
  );
};

export default CustomContainerForm;
