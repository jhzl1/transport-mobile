const Footer = () => {
  return (
    <footer className="mt-5">
      <h6 className="text-center fw-bold">
        Copyright © 2021 Corporación Unidigital. All rights reserved.
      </h6>
    </footer>
  );
};

export default Footer;
