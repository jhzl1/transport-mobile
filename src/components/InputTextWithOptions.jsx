import { Form, Input, Select } from "formik-antd";

const InputText = ({
  name,
  placeholder,
  nameSelect,
  placeholderSelect,
  disabled,
  options,
  maxLength,
  limitNumber,
  defaultValue,
}) => {
  const handleKeyDown = (e) => {
    if (e.key === " ") {
      e.preventDefault();
    }
  };
  const { Option } = Select;

  return (
    <div className="d-flex flex-row w-100">
      <Form.Item
        name={nameSelect}
        label={placeholderSelect}
        className="input me-2 w-25"
      >
        <Select
          name={nameSelect}
          placeholder={placeholderSelect}
          defaultValue={defaultValue && options[0].default.value}
        >
          {options[0].values.map((item, index) => (
            <Option value={item.value} key={index}>
              {item.name}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item name={name} label="  " className="input w-75">
        <Input
          name={name}
          maxLength={maxLength}
          className="w-100"
          placeholder={placeholder}
          allowClear
          type={limitNumber && "number"}
          onKeyDown={handleKeyDown}
          disabled={disabled && disabled}
        />
      </Form.Item>
    </div>
  );
};

export default InputText;
