import { Form, Input } from "formik-antd";

const InputPassword = ({ name, placeholder, maxLength }) => {
  const handleKeyDown = (e) => {
    if (e.key === " ") {
      e.preventDefault();
    }
  };

  return (
    <Form.Item name={name} label={placeholder} className="input w-100 me-2">
      <Input.Password
        name={name}
        placeholder={placeholder}
        maxLength={maxLength}
        onKeyDown={handleKeyDown}
        className="w-100"
      />
    </Form.Item>
  );
};

export default InputPassword;
