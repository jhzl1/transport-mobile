import * as Yup from "yup";
import { PhoneRegExPattern, regexEmail } from "../../../helpers/const";

export const validateEditCurrentUser = Yup.object({
  nombre: Yup.string()
    .required("El Nombre es requerido")
    .min(3, "El Nombre es muy corto"),
  correo: Yup.string()
    .email("No es un Email válido")
    .matches(regexEmail, "El dominio del Email no es válido")
    .required("El Email es requerido"),
  telefono: Yup.string()
    .matches(PhoneRegExPattern, "El Número de Teléfono no es válido")
    .min(11, "El Número de Teléfono debe tener 11 dígitos")
    .required("El Número de Teléfono es requerido")
    .typeError("Solo se admiten números"),
});
