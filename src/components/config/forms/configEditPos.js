import * as Yup from "yup";

export const validationNewPos = Yup.object({
  posType: Yup.string().required("Seleccione un Tipo de POS"),
});
