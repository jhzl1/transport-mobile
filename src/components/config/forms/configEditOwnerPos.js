import * as Yup from "yup";
import { PhoneRegExPattern, regexEmail } from "../../../helpers/const";

export const validationEditOwnerPos = Yup.object({
  email: Yup.string()
    .email("No es un Email válido")
    .matches(regexEmail, "El dominio del Email no es válido")
    .required("El Email es requerido"),
  phone: Yup.string()
    .matches(PhoneRegExPattern, "El Número de Teléfono no es válido")
    .min(11, "El Número de Teléfono debe tener 11 dígitos")
    .required("El Número de Teléfono es requerido")
    .typeError("Solo se admiten números"),
  fiscalAddress: Yup.string()
    .required("La Dirección Fiscal es requerida")
    .min(15, "La Dirección Fiscal es muy corta"),
  contactName: Yup.string()
    .required("El Nombre del Contacto es requerido")
    .min(3, "El Nombre del Contacto es muy corto"),
  contactLastName: Yup.string()
    .required("El Apellido del Contacto es requerido")
    .min(3, "El Apellido del Contacto es muy corto"),
  contactDocumentId: Yup.string()
    .matches(/^[1-9]\d*$/, "El Número de Cédula no es válido")
    .required("El Número de Cédula es requerido")
    .min(5, "El Número de Cédula debe ser mínimo de 5 dígitos"),
  contactPhone: Yup.string()
    .matches(PhoneRegExPattern, "El Número de Teléfono no es válido")
    .min(11, "El Número de Teléfono debe tener 11 dígitos")
    .required("El Teléfono del Contacto es requerido")
    .typeError("Sólo se admiten números"),
});
