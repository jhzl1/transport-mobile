import * as Yup from "yup";

export const initialValues = {
  serialNumber: "",
  PosModelStrongId: "",
  PosType: "",
};

export const validationNewPos = Yup.object({
  PosModelStrongId: Yup.string().required("Seleccione un Modelo"),
  PosType: Yup.string().required("Seleccione un Tipo"),
  serialNumber: Yup.string()
    .required("El Serial es requerido")
    .matches(/^[a-zA-Z0-9]+$/, "Solo se permiten caracteres alfanuméricos")
    .matches(/^[1-9]\d*$/, "El Serial debe empezar con dígitos entre 1 y 9")
    .matches(/(?!000000000)[0-9][0-9]{8}/g, "No es un Serial válido")
    .max(50, "Solo se permiten 50 caracteres"),
});
