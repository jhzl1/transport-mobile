import * as Yup from "yup";
import { regexEmail } from "../../../helpers/const";

export const gruopsUser = [
  {
    strongId: "value1",
    name: "Administrativo",
    id: 1,
  },
  {
    strongId: "value4",
    name: "POS Consumo",
    id: 2,
  },
  {
    strongId: "value5",
    name: "POS Recarga",
    id: 3,
  },
  {
    strongId: "value6",
    name: "API Pago",
    id: 4,
  },
];

export const validationNewUser = Yup.object({
  email: Yup.string()
    .email("No es un Email válido")
    .matches(regexEmail, "El dominio del Email no es válido")
    .required("El Email es requerido"),
  password: Yup.string()
    .required("La Contraseña es requerida")
    .oneOf([Yup.ref("repeatPassword")], "Las Contraseñas no coinciden")
    .min(8, "La Contraseña debe tener mínimo 8 caracteres"),
  repeatPassword: Yup.string()
    .required("La Contraseña es requerida")
    .oneOf([Yup.ref("password")], "Las Contraseñas no coinciden"),
  name: Yup.string()
    .required("El Nombre es requerido")
    .min(3, "El Nombre es muy corto"),
  surname: Yup.string()
    .required("El Apellido es requerido")
    .min(3, "El Apellido es muy corto"),
  group: Yup.string().required("Seleccione un Rol para el Usuario"),
});

export const initialValues = {
  email: "",
  password: "",
  repeatPassword: "",
  name: "",
  surname: "",
  group: "",
};
