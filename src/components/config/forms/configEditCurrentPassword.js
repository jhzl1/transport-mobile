import * as Yup from "yup";

export const validationEditCurentPassword = Yup.object({
  contraseñaVieja: Yup.string().required("La Contraseña Anterior es requerida"),
  nuevaContraseña: Yup.string()
    .required("La Contraseña es requerida")
    .oneOf([Yup.ref("repetirNuevaContraseña")], "Las Contraseñas no coinciden")
    .min(8, "La Contraseña debe tener mínimo 8 caracteres"),
  repetirNuevaContraseña: Yup.string()
    .required("La Contraseña es requerida")
    .oneOf([Yup.ref("nuevaContraseña")], "Las Contraseñas no coinciden"),
});
