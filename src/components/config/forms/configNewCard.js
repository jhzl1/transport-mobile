import * as Yup from "yup";

export const configNewCard = Yup.object({
  Number: Yup.string()
    .required("El Número de Tarjeta es requerido")
    .min(16, "El Número de Tarjeta debe tener 16 dígitos")
    .matches(/^[0-9]*$/, "Sólo se admiten números"),
});
