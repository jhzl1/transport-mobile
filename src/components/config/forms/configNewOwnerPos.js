import * as Yup from "yup";
import { PhoneRegExPattern, regexEmail } from "../../../helpers/const";

export const initialValues = {
  Name: "",
  FiscalRegistryType: "J",
  FiscalRegistry: "",
  FiscalAddress: "",
  AccountNumber: "",
  Email: "",
  Phone: "",
  ContactName: "",
  ContactLastName: "",
  TypeDocumentID: "",
  ContactDocumentIdType: "V",
  ContactDocumentId: "",
  ContactPhone: "",
};

export const validationNewOwnerPos = Yup.object({
  Name: Yup.string().required("La Razón Social es requerida"),
  FiscalRegistry: Yup.string()
    .required("El RIF es requerido")
    .typeError("Sólo se admiten números")
    .min(9, "El RIF debe tener un total de 9 dígitos")
    .matches(/(?!000000000)[0-9][0-9]{8}/g, "El RIF no es válido"),
  FiscalAddress: Yup.string()
    .required("La Dirección Fiscal es requerida")
    .min(15, "La Dirección Fiscal es muy corta"),
  AccountNumber: Yup.number()
    .required("El Número de Cuenta es requerido")
    .typeError("Sólo se admiten números")
    .min(20, "El Número de Cuenta debe ser mínimo de 20 dígitos"),
  Email: Yup.string()
    .email("No es un Email válido")
    .matches(regexEmail, "El dominio del Email no es válido")
    .required("El Email es requerido"),
  Phone: Yup.string()
    .matches(PhoneRegExPattern, "El Número de Teléfono no es válido")
    .required("El Número de Teléfono es requerido")
    .min(11, "El Número de Teléfono debe tener 11 dígitos")
    .typeError("Solo se admiten números"),
  ContactName: Yup.string()
    .required("El Nombre del Contacto es requerido")
    .min(3, "El Nombre del Contacto es muy corto"),
  ContactLastName: Yup.string()
    .required("El Apellido del Contacto es requerido")
    .min(3, "El Apellido del Contacto es muy corto"),
  TypeDocumentID: Yup.string(),
  ContactDocumentId: Yup.string()
    .matches(/^[1-9]\d*$/, "El Número de Cédula no es válido")
    .required("El Número de Cédula es requerido")
    .min(5, "El Número de Cédula debe ser mínimo de 5 dígitos"),
  ContactPhone: Yup.string()
    .matches(PhoneRegExPattern, "El Número de Teléfono no es válido")
    .min(11, "El Número de Teléfono debe tener 11 dígitos")
    .required("El Teléfono del Contacto es requerido")
    .typeError("Sólo se admiten números"),
});
