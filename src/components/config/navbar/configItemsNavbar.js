import * as FaIcons from "react-icons/fa";
import * as BiIcons from "react-icons/bi";
import * as MdIcons from "react-icons/md";
import { routes } from "../../Routes/routes";

const { posList, clousureList, userProfile, home } = routes;

export const itemsNavbar = [
  {
    id: 0,
    name: "Inicio",
    icon: <FaIcons.FaHome />,
    path: home,
  },
  {
    id: 1,
    name: "Cierres",
    icon: <BiIcons.BiStats />,
    path: clousureList,
  },
  {
    id: 2,
    name: "Mis POS",
    icon: <MdIcons.MdPhonelinkRing />,
    path: posList,
  },
  {
    id: 3,
    name: "Mi Perfil",
    icon: <BiIcons.BiUserCircle />,
    path: userProfile,
  },
];
