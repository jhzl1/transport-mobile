import { Badge, Descriptions, Button } from "antd";
import PosImage from "../assets/img/pos-terminal.png";
import { underConstruction } from "../helpers/modalUnderConstruction";

const DescriptionPos = ({ data }) => {
  const { id, name, type, status, serialNumber } = data;

  return (
    <div className="description">
      <div className="d-flex w-100 justify-content-center mb-3 description-image p-4 rounded">
        <div className="image-detail rounded-circle">
          <img src={PosImage} alt="Usuario" width="100px" />
        </div>
      </div>
      <Descriptions
        title="Datos del POS"
        column={1}
        layout="horizontal"
        extra={
          <Button
            className="me-1"
            type="primary"
            onClick={() => underConstruction()}
          >
            Ver Cierres
          </Button>
        }
        bordered
        className="py-3 px-1"
      >
        <Descriptions.Item label="ID">{id}</Descriptions.Item>
        <Descriptions.Item label="Modelo">{name}</Descriptions.Item>
        <Descriptions.Item label="Tipo">
          {type === 1 && "Ninguno"}
          {type === 2 && "Recarga"}
          {type === 3 && "Consumo"}
        </Descriptions.Item>
        <Descriptions.Item label="Estatus">
          {status === 1 && <Badge status="default" text="Nuevo" />}
          {status === 2 && <Badge color="#2db7f5" text="Asignado" />}
          {status === 3 && <Badge status="processing" text="Activo" />}
          {status === 4 && <Badge status="success" text="Bloqueado" />}
          {status === 5 && <Badge status="warning" text="Cancelado" />}
        </Descriptions.Item>
        <Descriptions.Item label="Serial">{serialNumber}</Descriptions.Item>
        <Descriptions.Item label="Comisión"> - </Descriptions.Item>
        <Descriptions.Item label="Último Cierre">-</Descriptions.Item>
      </Descriptions>
    </div>
  );
};

export default DescriptionPos;
