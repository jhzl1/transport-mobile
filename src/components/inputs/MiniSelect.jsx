import { Form, Select } from "formik-antd";

const MiniSelect = ({ name, options, placeholder }) => {
  const { Option } = Select;
  return (
    <Form.Item name={name} label={placeholder} className="input w-25 me-2">
      <Select name={name} placeholder={placeholder}>
        {options.map((item) => (
          <Option value={item.strongId} key={item.id}>
            {item.name}
          </Option>
        ))}
      </Select>
    </Form.Item>
  );
};

export default MiniSelect;
