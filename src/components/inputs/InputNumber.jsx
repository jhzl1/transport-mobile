import { Form, InputNumber as FieldNumber } from "formik-antd";

const InputNumber = ({ name, placeholder, maxLength }) => {
  return (
    <Form.Item name={name} label={placeholder} className="input w-100 me-2">
      <FieldNumber
        name={name}
        className="w-100"
        placeholder={placeholder}
        min={1}
        max={maxLength}
      />
    </Form.Item>
  );
};

export default InputNumber;
