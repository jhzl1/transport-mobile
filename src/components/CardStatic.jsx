import { Link } from "react-router-dom";
import { Card } from "antd";

const CardStatic = ({ title, value, img, path, pathName }) => {
  return (
    <Card
      actions={path && [<Link to={path}>Ver</Link>]}
      className="my-1 card-statics w-100"
    >
      <div className="d-flex flex-row">
        <img alt="POS Recarga" className="img-statics me-3" src={img} />
        <div>
          <h4>{title}</h4>
          <span className="fs-2 text-center">{value}</span>
        </div>
      </div>
    </Card>
  );
};

export default CardStatic;
