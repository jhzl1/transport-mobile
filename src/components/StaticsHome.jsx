// import { useState, useEffect } from "react";
// import axios from "../helpers/axiosInstance";
// import { BASE_URL } from "../services/api.js";
import CardStatic from "../components/CardStatic";
import posImage from "../assets/img/pos.png";
import amountImage from "../assets/img/amount.png";
import clousureMonth from "../assets/img/cash.png";

const StaticsHome = () => {
  // const [data, setData] = useState([]);

  // const getData = async () => {
  //   const res = await axios.get(`${BASE_URL}/api/home/dashboard`);
  //   setData(res.data.result);
  // };

  // console.log(data);

  // useEffect(() => {
  //   getData();
  //   return () => {
  //     setData([]);
  //   };
  // }, []);

  return (
    <div className="d-flex flex-row justify-content-center flex-wrap w-100">
      <CardStatic
        title="Total POS"
        value={1}
        img={posImage}
        path="/tpt/pos/list"
      />
      <CardStatic
        title="Cierres por Liquidar"
        value={1000000}
        img={amountImage}
        path="/tpt/clousureList"
      />
      <CardStatic title="Cierres de Mes" value={5000000} img={clousureMonth} />
    </div>
  );
};

export default StaticsHome;
