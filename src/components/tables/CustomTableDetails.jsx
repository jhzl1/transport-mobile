import { Table } from "antd";
import useSearchTable from "../../hooks/useSearchTable";
import axios from "../../helpers/axiosInstance";
import { useEffect, useState, useCallback } from "react";
import { BASE_URL } from "../../services/api.js";
import { useParams } from "react-router";
import EmptyResults from "../EmptyResults";

const CustomTableDetails = ({
  endpoint,
  rowKey = "strongId",
  configColumns,
}) => {
  const [data, setData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageNumber, setPageNumber] = useState(0);
  const [loading, setLoading] = useState(false);
  const [sizePage, setSizePage] = useState(10);

  const getColumnSearchProps = useSearchTable();
  const { strongId } = useParams();

  const getData = useCallback(
    async (params) => {
      // console.log(params);
      setLoading(true);
      try {
        const res = await axios.post(`${BASE_URL}/${endpoint}`, {
          number: pageNumber,
          size: sizePage,
          StrongId: strongId,
        });
        setData(res.data.result);
        console.log(res);
        setTotal(res.data.count);
      } catch (error) {
        console.log(error);
      }
      setLoading(false);
    },
    [endpoint, pageNumber, sizePage, strongId]
  );

  const handleTableChange = (pages, filters, sorter) => {
    // getData({
    //   sortField: sorter.field,
    //   sortOrder: sorter.order,
    //   pages,
    //   ...filters,
    // });
    setPageNumber(pages.current - 1);

    setSizePage(pages.pageSize);
  };

  useEffect(() => {
    let mounted = true;

    if (mounted) {
      getData();
    }

    return () => (mounted = false);
  }, [getData]);

  return (
    <>
      {data.length === 0 ? (
        <EmptyResults />
      ) : (
        <Table
          rowKey={rowKey}
          className="mt-4 table"
          scroll={{ x: "max-content" }}
          columns={configColumns(getColumnSearchProps, strongId)}
          expandable={{
            expandedRowRender: (record) => (
              <p className="mx-5 my-1">ID: {record.strongId}</p>
            ),
          }}
          dataSource={data}
          pagination={{ total }}
          loading={loading}
          onChange={handleTableChange}
          size="small"
        />
      )}
    </>
  );
};

export default CustomTableDetails;
