import { slide as MenuSlide } from "react-burger-menu";
import Burger from "@animated-burgers/burger-squeeze";
import "@animated-burgers/burger-squeeze/dist/styles.css";
import { useState } from "react";

import Sidebar from "../components/layout/Sidebar";

const MenuResponsive = () => {
  const [isOpen, setIsOpen] = useState(true);

  const changeOpen = () => {
    setIsOpen(!isOpen);
  };

  return (
    <MenuSlide
      isOpen={false}
      onStateChange={() => changeOpen()}
      customBurgerIcon={
        <Burger
          isOpen={isOpen}
          className={isOpen ? "bm-burger-button-active" : null}
        />
      }
    >
      <Sidebar isOpen={isOpen} changeOpen={changeOpen} />
    </MenuSlide>
  );
};

export default MenuResponsive;
