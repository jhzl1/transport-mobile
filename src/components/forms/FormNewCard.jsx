import CustomForm from "../CustomForm";
import { Formik } from "formik";
import InputText from "../InputText";
import { SubmitButton } from "formik-antd";
import { Col } from "react-bootstrap";
import { configNewCard } from "../config/forms/configNewCard";

const FormNewCard = ({ handleSubmit }) => {
  return (
    <Formik
      initialValues={{
        Number: "",
      }}
      validationSchema={configNewCard}
      onSubmit={handleSubmit}
    >
      <CustomForm>
        <InputText
          name="Number"
          placeholder="Número de Tarjeta"
          limitSpace
          maxLength={16}
        />

        <Col lg={12} className="d-flex justify-content-center">
          <SubmitButton type="primary" className="m-2" disabled={false}>
            Agregar Tarjeta
          </SubmitButton>
        </Col>
      </CustomForm>
    </Formik>
  );
};

export default FormNewCard;
