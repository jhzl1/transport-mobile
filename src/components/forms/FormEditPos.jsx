import { Formik } from "formik";
import InputSelect from "../InputSelect";
import CustomForm from "../CustomForm";
import { Row, Col } from "react-bootstrap";
import { SubmitButton } from "formik-antd";
import { posTypes } from "../../helpers/const";
import { checkModelValid } from "../../helpers/checkModelValid";

const FormEditPos = ({ handleSubmit, dataToEdit }) => {
  // console.log(checkModelValid(dataToEdit.name));

  return (
    <Formik
      initialValues={{
        strongId: dataToEdit.strongId,
        PosModelStrongId: checkModelValid(dataToEdit.name),

        PosType: dataToEdit.type,
      }}
      // validationSchema={validationNewDriver}
      onSubmit={handleSubmit}
    >
      <CustomForm>
        <Row>
          <Col>
            <div className="mb-4">
              <p>
                Modelo de POS:
                <span className="fw-bold"> {dataToEdit.name}</span>
              </p>
            </div>

            <InputSelect
              name="PosType"
              placeholder="Tipo de POS"
              options={posTypes}
            />
          </Col>

          <Col lg={12} className="d-flex justify-content-center">
            <SubmitButton type="primary" className="m-2">
              Guardar Cambios
            </SubmitButton>
          </Col>
        </Row>
      </CustomForm>
    </Formik>
  );
};

export default FormEditPos;
