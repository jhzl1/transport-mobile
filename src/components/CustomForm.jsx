import { Form } from "formik-antd";

const CustomForm = ({ children }) => {
  const layout = {
    layout: "vertical",
    labelCol: {
      span: 10,
    },
    wrapperCol: {
      span: 24,
    },
  };

  return (
    <Form
      className=" form pt-3 px-4 justify-content-center flex-wrap w-100"
      {...layout}
    >
      {children}
    </Form>
  );
};

export default CustomForm;
