import { Link } from "react-router-dom";
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from "react-pro-sidebar";
import { dataMenu } from "../data/dataMenu";
import { menuVariants } from "../data/dataAnimation";
import { motion } from "framer-motion";
import * as IoIcons from "react-icons/io5";
import HeaderSidebar from "./HeaderSidebar";

const MenuDesktop = () => {
  return (
    <motion.div
      variants={menuVariants}
      initial="initial"
      animate="enter"
      exit="exit"
      id="animated-menu"
    >
      <ProSidebar
        collapsed={false}
        className="d-none d-sm-none d-md-none d-lg-none d-xl-block"
      >
        <SidebarHeader className="sidebar-header">
          <HeaderSidebar />
        </SidebarHeader>
        <SidebarContent>
          <Menu iconShape="square" className="menu-desktop">
            <MenuItem icon={<IoIcons.IoDocuments />} className="mb-2">
              <Link to="/tpt/home">Resumen</Link>
            </MenuItem>
            {dataMenu.map((item) => (
              <SubMenu
                key={item.id}
                icon={item.icon}
                title={item.name}
                className="submenu mb-2"
              >
                {item.items &&
                  item.items.map((item, index) => (
                    <MenuItem key={index}>
                      <Link to={item.path}>{item.name}</Link>
                    </MenuItem>
                  ))}
              </SubMenu>
            ))}
          </Menu>
        </SidebarContent>
        <SidebarFooter></SidebarFooter>
      </ProSidebar>
    </motion.div>
  );
};

export default MenuDesktop;
