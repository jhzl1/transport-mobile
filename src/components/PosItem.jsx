import { Card } from "antd";
import posImage from "../assets/img/pos.png";
import * as BiIcons from "react-icons/bi";
import { Link } from "react-router-dom";

const { Meta } = Card;

// let loading = true;

const PosItem = () => {
  return (
    <Card
      className="my-1"
      actions={[
        <Link to="/tpt/pos/detail/2225ccdc-523f-46fb-aa76-4ee2eaa128b1">
          <span>
            <BiIcons.BiDetail className="mx-2" />
            Detalles
          </span>
        </Link>,
      ]}
    >
      {/* <Skeleton loading={loading} avatar active> */}
      <Meta
        avatar={<img src={posImage} width="60" alt="POS" />}
        title="Nombre POS"
        description="Subtítulo"
      />
      {/* </Skeleton> */}
    </Card>
  );
};

export default PosItem;
