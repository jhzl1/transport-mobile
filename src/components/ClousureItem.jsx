import { Card, Tooltip, Tag } from "antd";
import { Link } from "react-router-dom";
import * as AiIcons from "react-icons/ai";

const ClousureItem = ({ date, amount, id, status }) => {
  return (
    <Card
      className="my-1"
      title={
        <div className="d-flex flex-row justify-content-between">
          <h5 className="mb-0 p-2">{date}</h5>
          <span className="amout-clousure">{amount}</span>
        </div>
      }
      size="small"
    >
      <div className="d-flex flex-row justify-content-between">
        <span>ID: {id}</span>
        <div>
          <span>
            {status === 1 && <Tag color="#2db7f5">Nuevo</Tag>}
            {status === 2 && <Tag color="#8E44AD ">En Proceso</Tag>}
            {status === 3 && <Tag color="#00a859">Completado</Tag>}
          </span>
          <Tooltip title="Detalles">
            <Link to={`/tpt/clousure/detail`}>
              <span>
                <AiIcons.AiOutlineFileSearch className="mx-2" />
              </span>
            </Link>
          </Tooltip>
        </div>
      </div>
    </Card>
  );
};

export default ClousureItem;
