import { motion } from "framer-motion";
import { desktopVariants } from "../data/dataAnimation";

const ContainerEffect = ({ children }) => {
  return (
    <motion.div
      variants={desktopVariants}
      initial="initial"
      animate="enter"
      exit="exit"
      className="desktop-container mt-2 mb-5"
    >
      {children}
    </motion.div>
  );
};

export default ContainerEffect;
