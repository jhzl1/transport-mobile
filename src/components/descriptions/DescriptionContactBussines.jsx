import { Descriptions } from "antd";
import userImg from "../../assets/img/user.png";
import { useState } from "react";

const DescriptionContactBussines = ({ data }) => {
  const [layoutDescription, setLayoutDescription] = useState("horizontal");

  const checkResize = () => {
    window.innerWidth <= 900
      ? setLayoutDescription("vertical")
      : setLayoutDescription("horizontal");
  };

  window.addEventListener("resize", checkResize);

  return (
    <div className="description my-3">
      <div className="d-flex w-100 justify-content-center">
        <div className="image-detail">
          <img src={userImg} alt="Centro de recarga" width="100px" />
        </div>
      </div>
      <Descriptions
        title="Detalles del Contacto"
        column={1}
        layout={layoutDescription}
        bordered
      >
        <Descriptions.Item label="Nombre Completo">
          {data.contactName + " " + data.contactLastName}
        </Descriptions.Item>
        <Descriptions.Item label="Cédula">
          {data.contactDocumentId}
        </Descriptions.Item>
        <Descriptions.Item label="Teléfono">
          {data.contactPhone}
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

export default DescriptionContactBussines;
