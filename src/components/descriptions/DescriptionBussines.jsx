import { Descriptions } from "antd";
import companyImg from "../../assets/img/company.png";
import { useState } from "react";

const DescriptionBussines = ({ data }) => {
  const [layoutDescription, setLayoutDescription] = useState("horizontal");

  const {
    name,
    fiscalRegistry,
    fiscalAddress,
    accountNumber,
    phone,
    email,
    created,
  } = data;

  const checkResize = () => {
    window.innerWidth <= 900
      ? setLayoutDescription("vertical")
      : setLayoutDescription("horizontal");
  };

  window.addEventListener("resize", checkResize);

  return (
    <div className="description my-3">
      <div className="d-flex w-100 justify-content-center">
        <div className="image-detail">
          <img src={companyImg} alt="Empresa" width="100px" />
        </div>
      </div>
      <Descriptions
        title="Datos Básicos"
        column={1}
        layout={layoutDescription}
        bordered
      >
        <Descriptions.Item label="Razón Social">{name}</Descriptions.Item>
        <Descriptions.Item label="RIF">{fiscalRegistry}</Descriptions.Item>
        <Descriptions.Item label="Dirección Fiscal">
          {fiscalAddress}
        </Descriptions.Item>
        <Descriptions.Item label="Número de Cuenta">
          {accountNumber}
        </Descriptions.Item>
        <Descriptions.Item label="Teléfono">{phone}</Descriptions.Item>
        <Descriptions.Item label="Email">{email}</Descriptions.Item>
        {/* <Descriptions.Item label="Número de Cuenta">
          {idPos}
        </Descriptions.Item> */}
        <Descriptions.Item label="Fecha de Creación">
          {new Date(created).toLocaleDateString("en-gb")}
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

export default DescriptionBussines;
