import { Input, Form } from "antd";

const DescriptionUser = () => {
  return (
    <Form layout="vertical">
      <Form.Item label="Razón Social">
        <Input value="hola" disabled />
      </Form.Item>
      <Form.Item label="RIF">
        <Input value="J-3322445455" disabled />
      </Form.Item>
      <Form.Item label="Número de Cuenta">
        <Input value="0102335656655" disabled />
      </Form.Item>
      <Form.Item label="Nombre Completo">
        <Input value="Juanito Pérez" disabled />
      </Form.Item>
      <Form.Item label="Número de Cédula">
        <Input value="V-88778992" disabled />
      </Form.Item>
      <Form.Item label="Comisión POS">
        <Input value="21" disabled />
      </Form.Item>
      <Form.Item label="Comisión Consumo">
        <Input value="5" disabled />
      </Form.Item>
    </Form>
  );
};

export default DescriptionUser;
