import { Descriptions } from "antd";
import analyticsImg from "../../assets/img/analytics.png";

const DescriptionClousure = () => {
  return (
    <div className="description my-3">
      <div className="d-flex w-100 justify-content-center mb-5 ">
        <div className="image-detail rounded-circle">
          <img src={analyticsImg} alt="Estadísticas" width="100px" />
        </div>
      </div>
      <Descriptions column={1} layout="horizontal" bordered>
        <Descriptions.Item label="Fecha del Cierre">
          10/12/2021
        </Descriptions.Item>
        <Descriptions.Item label="Comisión">20%</Descriptions.Item>
        <Descriptions.Item label="Monto">1.000.000</Descriptions.Item>
        <Descriptions.Item label="Cuenta">
          01023326565626266686
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

export default DescriptionClousure;
