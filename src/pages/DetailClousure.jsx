import ContainerEffect from "../components/ContainerEffect";
import { PageHeader, Tabs } from "antd";
import { useHistory } from "react-router";
import DescriptionClousure from "../components/descriptions/DescriptionClousure";

const DetailClousure = () => {
  const history = useHistory();

  const { TabPane } = Tabs;

  return (
    <ContainerEffect>
      <PageHeader
        className="site-page-header px-3"
        ghost={false}
        onBack={() => history.goBack()}
        title="Detalles de Cierre"
      >
        <Tabs defaultActiveKey="0">
          <TabPane tab="General" key="0">
            <DescriptionClousure />
          </TabPane>
          <TabPane tab="Tarjetas" key="1" disabled></TabPane>
        </Tabs>
      </PageHeader>
    </ContainerEffect>
  );
};

export default DetailClousure;
