import { Tabs, notification } from "antd";
import FormChangePassword from "../components/FormChangePassword";
import DescriptionUser from "../components/descriptions/DescriptionUser";
import CustomContainer from "../components/CustomContainer";

const DetailUser = () => {
  const { TabPane } = Tabs;

  const handleSubmit = async (data) => {
    console.log(data);
    notification.open({
      message: "En Desarrollo",
      description:
        "La funcionalidad que acaba de testear se encuentra actualmente en desarrollo.",
    });
  };

  return (
    <CustomContainer>
      <div className="px-1 py-2">
        <Tabs className="form-user-logged px-3 py-2">
          <TabPane tab="Datos Generales" key="1">
            <DescriptionUser />
          </TabPane>
          <TabPane tab="Cambiar Contraseña" key="2">
            <FormChangePassword handleSubmit={handleSubmit} />
          </TabPane>
        </Tabs>
      </div>
    </CustomContainer>
  );
};

export default DetailUser;
