import CustomContainer from "../components/CustomContainer";
import PosItem from "../components/PosItem";

const PosList = () => {
  return (
    <CustomContainer title="Mis POS" push="/pos/new">
      <PosItem />
      <PosItem />
      <PosItem />
      <PosItem />
      <PosItem />
      <PosItem />
      <PosItem />
      <PosItem />
    </CustomContainer>
  );
};

export default PosList;
