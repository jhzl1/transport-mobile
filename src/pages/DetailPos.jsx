import ContainerEffect from "../components/ContainerEffect";
import { PageHeader } from "antd";
import { useHistory } from "react-router";

import DescriptionPos from "../components/DescriptionPos";
import { usePostDetail } from "../hooks/usePostDetail";
import SkeletonDescriptionCard from "../components/skeletons/SkeletonDescriptionCard";

const DetailPos = () => {
  const history = useHistory();

  const [data, loading] = usePostDetail("/api/pos/details");

  return (
    <ContainerEffect>
      <PageHeader
        className="site-page-header"
        ghost={false}
        onBack={() => history.goBack()}
        title="Detalles POS"
      >
        {loading && <SkeletonDescriptionCard />}
        {data && <DescriptionPos data={data} />}
      </PageHeader>
    </ContainerEffect>
  );
};

export default DetailPos;
