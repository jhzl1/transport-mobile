import CustomContainer from "../components/CustomContainer";
import CustomTable from "../components/CustomTable";
import { columnsTablePayments } from "../components/config/tables/configTablePayments";

const PaymentsList = () => {
  return (
    <CustomContainer title="Pagos">
      <CustomTable
        endpoint="/api/payment/list"
        rowKey="strongId"
        configColumns={columnsTablePayments}
      />
    </CustomContainer>
  );
};

export default PaymentsList;
