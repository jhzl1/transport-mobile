import ClousureItem from "../components/ClousureItem";
import CustomContainer from "../components/CustomContainer";

const ClousureList = () => {
  return (
    <CustomContainer title="Cierres">
      <ClousureItem id="55" amount="10.555,56" date="02/01/21" status={1} />
      <ClousureItem id="56" amount="5.555,56" date="02/01/21" status={2} />
      <ClousureItem id="57" amount="60.556.363,66" date="02/01/21" status={3} />
      <ClousureItem id="58" amount="16.555,56" date="02/01/21" status={1} />
      <ClousureItem id="57" amount="5.556.363,66" date="02/01/21" status={3} />
    </CustomContainer>
  );
};

export default ClousureList;
