import StaticsHome from "../components/StaticsHome";
import CustomContainer from "../components/CustomContainer";
import Footer from "../components/Footer";

const Home = () => {
  return (
    <>
      <CustomContainer>
        <StaticsHome />
      </CustomContainer>
      <Footer />
    </>
  );
};

export default Home;
