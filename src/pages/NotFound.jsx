import { Link, useHistory } from "react-router-dom";
import Error from "../assets/img/error.png";

const NotFound = () => {
  const history = useHistory();

  return (
    <div className="container p-4 d-flex flex-row">
      <img src={Error} alt="" style={{ width: "100px" }} className="me-4" />
      <div>
        <h2>¡Ups!</h2>
        <span className="fs-5">
          La página que intentas buscar no existe. Da click
          <Link onClick={() => history.goBack()}> aquí </Link> para regresar
          atrás
        </span>
      </div>
    </div>
  );
};

export default NotFound;
