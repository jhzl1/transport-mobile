import logoBdv from "../assets/img/logoBDV.png";
import { Redirect } from "react-router";
import FormLogin from "../components/FormLogin";
import { motion } from "framer-motion";
import { loginVariants } from "../data/dataAnimation";
import useAuth from "../hooks/useAuth";
import { Row, Col } from "react-bootstrap";

const Login = () => {
  const auth = useAuth();

  const handleSubmit = async (user) => {
    // await auth.login(user);
    auth.isLogged();
  };

  if (auth.testLogin) {
    return <Redirect to="/tpt/home" />;
  }

  return (
    <motion.div
      className="d-flex general-login"
      variants={loginVariants}
      initial="initial"
      animate="enter"
      exit="exit"
    >
      <motion.div className="login">
        <Row className="ms-0 me-0">
          <Col className="d-flex ps-0 pe-0 " md={12} lg={12} xl={6}>
            <div className="d-flex flex-fill justify-content-center align-items-center container-login-left p-5">
              <img
                src={logoBdv}
                alt="Banco de Venezuela"
                className="img-login"
              />
            </div>
          </Col>
          <Col
            id="container-login"
            md={12}
            lg={12}
            xl={6}
            className="ps-0 pe-0"
          >
            <div className="d-flex flex-column flex-fill  align-items-center">
              <h3 className="mt-4">Mi PasajeBDV</h3>

              <FormLogin handleSubmit={handleSubmit} />
            </div>
          </Col>
        </Row>
      </motion.div>
    </motion.div>
  );
};

export default Login;
