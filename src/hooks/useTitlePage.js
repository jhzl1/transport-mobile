import { useState, useEffect, useCallback } from "react";
import { useLocation } from "react-router-dom";

export const useTitlePage = () => {
  const [titleHeader, setTitleHeader] = useState("");

  const location = useLocation();
  const { pathname } = location;

  const changeTitle = useCallback(() => {
    if (pathname === "/tpt/clousureList") {
      setTitleHeader("Cierres");
    } else if (pathname === "/tpt/pos/list") {
      setTitleHeader("Mis POS");
    } else if (pathname === "/tpt/user/") {
      setTitleHeader("Mi Perfil");
    } else if (pathname === "/tpt/home") {
      setTitleHeader("Inicio");
    } else {
      setTitleHeader("Unicodigital");
    }
  }, [pathname]);

  useEffect(() => {
    changeTitle();
  }, [changeTitle]);

  return titleHeader;
};
