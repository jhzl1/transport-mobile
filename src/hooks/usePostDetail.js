import { useState, useEffect, useCallback } from "react";
import axios from "../helpers/axiosInstance";
import { BASE_URL } from "../services/api";
import { useParams } from "react-router";

export const usePostDetail = (url) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);

  const { strongId } = useParams();

  const getData = useCallback(async () => {
    setLoading(true);
    try {
      const res = await axios.post(`${BASE_URL}${url}`, {
        StrongId: strongId,
      });
      setData(res.data.result);
      console.log(res);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  }, [strongId, url]);

  useEffect(() => {
    getData();

    return () => setData(null);
  }, [getData]);

  return [data, loading];
};
