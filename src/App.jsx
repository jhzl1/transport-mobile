import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Desktop from "./components/layout/Desktop";
import NotFound from "./pages/NotFound";
import Login from "./pages/Login";
import PrivateRoute from "./components/Routes/PrivateRoute";
import { AnimatePresence } from "framer-motion";
import AuthProvider from "./components/auth/AuthProvider";
import { ConfigProvider } from "antd";
import esES from "antd/lib/locale/es_ES";
import { routes } from "./components/Routes/routes";
import { dirRoutes } from "./components/Routes/dir.routes";

function App() {
  const { login } = routes;

  return (
    <ConfigProvider locale={esES}>
      <AuthProvider>
        <Router basename="/tptMobile">
          <Route
            render={({ location }) => (
              <AnimatePresence exitBeforeEnter>
                <Switch location={location} key={location.pathname}>
                  {/* Home */}
                  <Route exact path={login} component={Login} />

                  <Desktop location={location}>
                    {dirRoutes.map((item) => (
                      <PrivateRoute
                        key={item.id}
                        exact
                        path={item.path}
                        component={item.component}
                      />
                    ))}
                  </Desktop>
                  <Route path="*" component={NotFound} />
                </Switch>
              </AnimatePresence>
            )}
          />
        </Router>
      </AuthProvider>
    </ConfigProvider>
  );
}

export default App;
