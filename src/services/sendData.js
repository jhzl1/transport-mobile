import { BASE_URL } from "./api";
import axios from "../helpers/axiosInstance";

export async function sendData(url, data) {
  try {
    const response = await axios.post(`${BASE_URL}${url}`, data);
    return response;
  } catch (e) {
    // console.clear();
    // console.log(Object.keys(e));
    // console.log(e.response.data.errors);
    return e.response;
  }
}

// {
//   headers: {
//     Authorization: `Bearer ${localStorage.getItem("jwt")}`,
//   },
// }
