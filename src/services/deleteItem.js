import { BASE_URL } from "./api";
import axios from "../helpers/axiosInstance";
import { message } from "antd";

const key = "updatable";

export const deleteItem = async (url, strongId, successMessage) => {
  message.loading({ content: "Cargando...", key });
  try {
    const res = await axios.delete(`${BASE_URL}${url}`, {
      data: { StrongId: strongId },
    });
    message.success({ content: successMessage, key, duration: 3 });
    console.log(res);

    setTimeout(() => {
      window.location.reload();
    }, 1500);

    return res;
  } catch (e) {
    message.error({
      content: e.response.data.errors[0].message,
      key,
      duration: 3,
    });
    console.log(e.response);
    return e.response;
  }
};
