import _axios from "axios";

const CSRF_COOKIE_NAME = "csrftoken";
const CSRF_HEADER_NAME = "X-CSRFToken";

const axios = _axios.create({
  baseURL: process.env.REACT_TRANSPORT_API_URL,
  timeout: 25000,
  responseType: "json",
  xsrfCookieName: CSRF_COOKIE_NAME,
  xsrfHeaderName: CSRF_HEADER_NAME,
  headers: {
    "Content-Type": "application/json",
  },
});

export default axios;
