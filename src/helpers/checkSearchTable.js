export const checkSearchTable = (params) => {
  if (params.number) {
    return {
      name: "number",
      value: params.number[0].toString(),
    };
  } else if (params.serialNumber) {
    return {
      name: "serialNumber",
      value: params.serialNumber[0].toString(),
    };
  } else if (params.id) {
    return {
      name: "Id",
      value: params.id[0].toString(),
    };
  } else if (params.reference) {
    return {
      name: "Reference",
      value: params.reference[0].toString(),
    };
  }
};
