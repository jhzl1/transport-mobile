import currency from "currency.js";

export const posTypes = [
  {
    id: 1,
    strongId: 1,
    name: "Ninguno",
  },
  {
    id: 2,
    strongId: 2,
    name: "Recarga",
  },
  {
    id: 3,
    strongId: 3,
    name: "Consumo",
  },
];

export const VES = (value) =>
  currency(value, { symbol: "", decimal: ",", separator: "." });

export const PhoneRegExPattern =
  /^(?:(?:00|\+)58|0)(?:2(?:12|4[0-9]|5[1-9]|6[0-9]|7[0-8]|8[1-35-8]|9[1-5]|3[45789])|4(?:1[246]|2[46]))\d{7}$/;

export const regexEmail = new RegExp(
  /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i
);

export const typesDocuments = [
  {
    default: { value: "V", name: "V" },
    values: [
      { value: "V", name: "V" },
      { value: "E", name: "E" },
    ],
  },
];

export const typesRif = [
  {
    default: { value: "J", name: "J" },
    values: [
      { value: "V", name: "V" },
      { value: "E", name: "E" },
      { value: "J", name: "J" },
      { value: "G", name: "G" },
    ],
  },
];
