import { notification } from "antd";

export const underConstruction = () => {
  notification.info({
    message: "En Desarrollo",
    description:
      "La funcionalidad que acaba de testear se encuentra actualmente en desarrollo.",
  });
};
