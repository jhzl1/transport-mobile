### Unicodigital Transporte

Esta aplicación fue creada con [Create React App](https://github.com/facebook/create-react-app).

## Importante:

Antes de ejecutar los comandos que se describirán a continuación, se debe ejecutar el comando `npm install` para que se instalen las dependencias con las que funciona el proyecto. De lo contrario, no funcionará.

## Comandos

### `npm start`

Corre la aplicación en modo de desarrollo.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

### `npm run build`

Compila la aplicación en la carpeta `build` lista para producción.\

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

Front-End desarrollado por Omar Campos. [Github](https://github.com/jhzl1)
